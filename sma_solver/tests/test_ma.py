from ..ma import SMA, get_ma_values


def test_sma():
    ma = SMA(3)
    values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    sma_values = list(get_ma_values(ma, values))
    assert sma_values == [1.0, 1.5, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0]


def test_sma_with_another_period():
    ma = SMA(2)
    values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    sma_values = list(get_ma_values(ma, values))
    assert sma_values == [1.0, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5]


def test_sma_with_period_1_should_be_the_same():
    ma = SMA(1)
    values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    sma_values = list(get_ma_values(ma, values))
    assert sma_values == [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0]
