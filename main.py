import sys

import settings
from sma_solver.exceptions import ValidationError
from sma_solver.gsheet_api import get_or_create_ma_col, open_sheet
from sma_solver.ma import SMA, get_ma_values
from sma_solver.validation import validate_data, validate_labels_and_values, validate_simple_label


def process_gsheet_data(sheet_id):
    try:
        # Open and validate everything

        sheet = open_sheet(settings.GOOGLE_AUTH_FILE, sheet_id)

        labels, values = validate_labels_and_values(sheet.get_all_values())

        # Find all needed columns
        visitors_idx = validate_simple_label(labels, 'Visitors')
        date_idx = validate_simple_label(labels, 'Date')
        ma_idx = get_or_create_ma_col(sheet, labels) + 1

        total_rows = len(values)

        values = validate_data(values, settings.DATE_PARSING_FORMAT, date_idx, visitors_idx)

        # Create function for calculating SMAs
        sma = SMA(settings.SMA_PERIOD)
        sma_values = get_ma_values(sma, map(lambda x: x[visitors_idx], values))

        start_row = 2
        batch_size = settings.UPDATE_BATCH_SIZE

        # Get initial Range to update
        cells = sheet.range(start_row, ma_idx, start_row + batch_size, ma_idx)
        cell_i = 0

        for i, val in enumerate(sma_values):
            row = start_row + i
            cells[cell_i].value = str(val).replace('.', ',')
            cell_i += 1

            # Update cells, if we proceed settings.UPDATE_BATCH_SIZE rows
            # OR if proceed is last row
            if cell_i >= batch_size or i == total_rows - 1:
                sheet.update_cells(cells)
                # If there are rows left - fetch new cells to update
                if i != total_rows - 1:
                    cell_i = 0
                    cells = sheet.range(row + 1, ma_idx, row + 1 + batch_size, ma_idx)

    except ValidationError as e:
        print(f'Error: {e.msg}')
        sys.exit(1)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        sheet_id = sys.argv[1]
    else:
        sheet_id = settings.DEFAULT_SHEET_ID
    process_gsheet_data(sheet_id)
