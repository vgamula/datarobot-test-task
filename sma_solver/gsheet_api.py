import gspread
from oauth2client.service_account import ServiceAccountCredentials

from .exceptions import ValidationError


def open_sheet(keyfile, sheet_id):
    # Open google spreedsheet by sheet_id
    try:
        credentials = ServiceAccountCredentials.from_json_keyfile_name(keyfile, ['https://spreadsheets.google.com/feeds'])
        client = gspread.authorize(credentials)
        worksheet = client.open_by_key(sheet_id)
        return worksheet.sheet1
    except Exception:
        raise ValidationError('There is an error during fetching the sheet, please check sheet-id and your credentials')


def get_or_create_ma_col(sheet, labels):
    # Check, whether there is column, named "Moving Average", if not - create such column
    text = 'Moving Average'
    if text in labels:
        return labels.index(text)
    new_col = len(labels)
    sheet.update_cell(1, new_col + 1, text)
    return new_col
