import os

get = os.environ.get

DEFAULT_SHEET_ID = get('DEFAULT_SHEET_ID', '1oT256doulovBZ2RAJijeq9JvovdB5JEublTTsSBpLkk')
GOOGLE_AUTH_FILE = get('GOOGLE_AUTH_FILE', 'DataRobot Test Task-50375d166b12.json')
DATE_PARSING_FORMAT = get('DATE_PARSING_FORMAT', '%d.%m.%Y')
SMA_PERIOD = int(get('SMA_PERIOD', 3))
UPDATE_BATCH_SIZE = int(get('UPDATE_BATCH_SIZE', 15))
