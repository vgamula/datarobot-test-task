from collections import deque


class SMA:

    def __init__(self, period):
        self.period = period
        self.values = deque()  # lets use queue for storing current values

    def __call__(self, x):
        self.values.append(x)
        if len(self.values) > self.period:
            # Remove last value when the length of values is bigger than period
            self.values.popleft()
        return sum(self.values) / len(self.values)  # calculate average


def get_ma_values(sma_func, values):
    # Helper function for generating SMA-values
    for val in values:
        yield sma_func(val)
