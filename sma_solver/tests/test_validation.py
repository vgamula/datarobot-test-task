import pytest

from ..validation import validate_data, validate_labels_and_values, validate_simple_label
from ..exceptions import ValidationError


@pytest.mark.parametrize('labels,needed_label', [
    (['Id', 'Date'], 'Date'),
    (['Id', 'Date', 'Visitors'], 'Visitors'),
    (['Moving Average'], 'Moving Average'),
])
def test_validate_simple_label_success(labels, needed_label):
    res = validate_simple_label(labels, needed_label)
    assert isinstance(res, int)


@pytest.mark.parametrize('labels,needed_label', [
    (['Id'], 'Date'),
    (['Id', 'Date'], 'Visitors'),
    (['Moving Average'], 'Date'),
])
def test_validate_simple_label_fail(labels, needed_label):
    with pytest.raises(ValidationError):
        validate_simple_label(labels, needed_label)


@pytest.mark.parametrize('data', [
    ([['Id', 'Date', 'Visitors'], ['1', '12.10.2000', '17']]),
    ([['Id', 'Date', 'Visitors'], ['1', '12.10.2000', '12'], ['2', '12.02.2000', '23']]),
])
def test_validate_labels_and_values_success(data):
    labels, values = validate_labels_and_values(data)
    assert isinstance(labels, list)
    assert isinstance(values, list)


@pytest.mark.parametrize('data', [
    ([['Id', 'Date', 'Visitors']]),
    ([]),
])
def test_validate_labels_and_values_fail(data):
    with pytest.raises(ValidationError):
        validate_labels_and_values(data)


def test_validate_data_success():
    date_format = '%d.%m.%Y'
    values = [
        ['1', '12.10.2000', '12'],
        ['2', '12.02.2000', '23'],
    ]
    assert ValidationError(validate_data(values, date_format, 1, 2))


@pytest.mark.parametrize('values,date_format,date_index,visitors_index', [
    (  # Wrong Date Index
        [
            ['1', '12.10.2000', '12'],
            ['2', '12.02.2000', '23'],
        ],
        '%d.%m.%Y',
        0,
        2,
    ),
    (  # Wrong Visitors Index
        [
            ['1', '12.10.2000', '12'],
            ['2', '12.02.2000', '23'],
        ],
        '%d.%m.%Y',
        1,
        1,
    ),
    (  # Wrong Date Format
        [
            ['1', '12.15.2000', '12'],
            ['2', '12.15.2000', '23'],
        ],
        '%d.%m.%Y',
        1,
        2,
    ),
])
def test_validate_data_fail(values, date_format, date_index, visitors_index):
    with pytest.raises(ValidationError):
        validate_data(values, date_format, date_index, visitors_index)
