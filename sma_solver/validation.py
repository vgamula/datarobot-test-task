from datetime import datetime

from gspread.utils import rowcol_to_a1

from .exceptions import ValidationError


def validate_data(values, date_format, date_index, visitors_index):
    # Check dates to be proper and correct visitors values
    for i, row in enumerate(values):
        row_number = i + 1
        try:
            row[date_index] = datetime.strptime(row[date_index], date_format)
        except (IndexError, ValueError, TypeError):
            raise ValidationError('Date parsing error at {}'.format(rowcol_to_a1(row_number + 1, date_index + 1)))
        try:
            row[visitors_index] = int(row[visitors_index])
        except (IndexError, ValueError, TypeError):
            raise ValidationError('Visitors parsing error at {}'.format(rowcol_to_a1(row_number + 1, date_index + 1)))
    return values


def validate_labels_and_values(data):
    # Check, whether we have enough values to proceed
    if len(data) <= 1:
        raise ValidationError('Please fill in all the required information')
    labels = data[0]
    values = data[1:]
    return labels, values


def validate_simple_label(labels, needed_label):
    # Check, whether we have some column in our data
    if needed_label not in labels:
        raise ValidationError('Column with label {} does not exist'.format(needed_label))
    return labels.index(needed_label)
