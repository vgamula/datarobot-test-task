# Home test task for DataRobot

#### Requiremets:
- Python 3.6

#### Install requirements
```
$ pip install -r requirements.txt`
```

#### Run application:
```
$ python main.py [sheet_id] 
```
Where `sheet_id` is your Google Sheet ID. It is not required, if you don't specify it - application will use default one, which is set in `settings.py`.

#### Options:
- GOOGLE_AUTH_FILE - path to settings file, with Google API credentials, if you wonder, how to get it - look at this [tutorial](http://gspread.readthedocs.io/en/latest/oauth2.html).
- DATE_PARSING_FORMAT - Format (pythonic style) for checking dates in the sheet, default is `%d.%m.%Y`.
- SMA_PERIOD - period for calculating Simple Moving Average, default is 3
- UPDATE_BATCH_SIZE - batch size for updating columns in google sheet, default is 15.

**Note: You can pass any of these parameters as ENV variable.**
**`SMA_PERIOD=5 python main.py 1oT256doulovBZ2RAJijeq9JvovdB5JEublTTsSBpLkk`**

#### Run tests:
```
$ py.test
```

### Demo Spreadsheets:
- **https://docs.google.com/spreadsheets/d/1oT256doulovBZ2RAJijeq9JvovdB5JEublTTsSBpLkk/edit#gid=0** id = 1oT256doulovBZ2RAJijeq9JvovdB5JEublTTsSBpLkk
- **https://docs.google.com/spreadsheets/d/12T8b-6RmSxy7hbPFCbppgHZ_u_mm_KOGsitt10EAyHM/edit#gid=0** id = 12T8b-6RmSxy7hbPFCbppgHZ_u_mm_KOGsitt10EAyHM

If you wnat to use your spreadsheet, do not forget to share access to it with email, which is set in Google Auth credentials by 'client_email' key.
